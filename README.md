# Implementing Less than Best Effort with Deadlines
*One LBE to rule them all and to the deadline bind them*

Master thesis on implementing Less than Best Effort with deadlines in the Linux Kernel. Builds on the framework proposal composed by David A. Hayes, David Ros, Andreas Petlund, and Iffat Ahmed.

## Abstract

Applications which do not carry strong capacity or latency constraints, such as backups, could use a Less than Best Effort (LBE) transport protocol as an alternative to best-effort TCP. This could minimize their potential to disrupt network flows with a higher significance. Existing LBE CCs, such as LEDBAT, enforce LBE behaviour without any timeliness requirements regarding completion time.

This paper introduces a framework API in the Linux Kernel that provides the ability to impose timeliness requirements and LBE behaviour on an arbitrary CC. The framework measures the degree of network congestion based on the most commonly used metrics of network congestion (loss, delay, and explicit congestion notification). The framework introduces functionality which provides the ability for network traffic to adjust its relative share of network capacity on a per-flow level. This functionality should be used to control the level of LBEness based on the congestion level in the network, as well as the relative time until the contracted completion time.

The implementation in the Linux Kernel builds on the framework proposal composed by *David A. Hayes, David Ros, Andreas Petlund, and Iffat Ahmed*. The framework proposal includes a solid foundation for the implemented framework. The effectiveness of the approach in the framework proposal is validated by numerical and emulation experiments. The effectiveness of the implemented framework is validated by simulation experiments. Both the emulation and simulation experiments use TCP Cubic or TCP Vegas as a Congestion Control (CC) mechanism.

## Authors

* **Lars Erik Storbukås** [(larserik.storbukas@gmail.com)](mailto:larserik.storbukas@gmail.com)

See also the [NEAT-project](https://www.neat-project.org/), which this thesis is a contribution to.
