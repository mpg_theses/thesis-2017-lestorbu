\chapter{Experimental setup}
\label{chapter:experimental-setup}

This chapter describes the experimental environment that have been configured to perform our measurements, including traffic-shaper, background-traffic, host running the DA-LBE features and the tools we used.

This section explores the technical configuration for our experimental environment, where we have performed our experiements and measurements. The steps neccesary in order to configure the experiemental setup is also presented.


\section{Hardware}
\label{subsection:hardware}

The experimental setup includes four Linux computers running Debian, a router to route the traffic and ethernet cables to connect them together. Each computer has a special role in our experimental setup, \cref{figure:testbed} gives an overview of the setup, and \cref{table:machine-overview} describes the purpose of the different computers.

\begin{figure}[H]
  \includegraphics[width=0.95\textwidth,keepaspectratio]{graphics/misc/testbed-setup}
  \caption{Hardware setup}
  \label{figure:testbed}
\end{figure}

\begin{table}[H]
  \centering
  \begin{tabularx}{\textwidth}{|Sl|X|} \hline
    \textbf{Machine} & \textbf{Purpose} \\ \hline
    DA-LBE & \footnotesize{Send data to \textbf{Destination} with DA-LBE enabled} \\ \hline
    Background traffic & \footnotesize{Send background traffic to \textbf{Destination}} \\ \hline
    Traffic shaper & \footnotesize{Rate limiting and delay} \\ \hline
    Destination & \footnotesize{Receive data from \textbf{DA-LBE} and \textbf{Background traffic} computers} \\ \hline
  \end{tabularx}
  \caption{Machine overview}
  \label{table:machine-overview}
\end{table}

In \cref{figure:testbed} we observe that the traffic shaping computer have two network interface controllers (NICs) in order to be able to shape the traffic between the router and the destination host. We could have managed to solve it by only using one, since most NICs support full-duplex connection, but chose to limit the amount of influencing factors in our experimental setup. The NICs of each computer have potential to carry traffic at the nominal rate of 100 Mbit/s (100BASE-TX).


\subsection{Configuring two network interfaces}
\label{subsubsection:two-interfaces}

In order for the traffic shaping computer to limit the transmission rate with minimal amount of influencing factors, we have equipped the traffic shaping computer with an additional NIC. The data arriving at one NIC is passed along to the second NIC before reaching the destination. The data transmitted keeps its integrity while traveling through the traffic shaper, but experiences a bottle neck (rate limiting) as well as added network delay in order to be more realistic. This configuration between the two NICs is commonly referred to as a network bridge. We are using the Linux network utility bridge-utils \cite{linux-bridge-utils} to bridge the two NICs together.

Both the rate limiting and added delay is achieved with NetEm. A challenge when using NetEm is to be able to achieve rate limiting and network delay simultaneously on a single NIC, as it is challenging to configure it properly. The rate limiting and network delay configuration can be configured properly in our case is by setting it on two different NICs. Consequently, any traffic traversing the traffic shaper, experiences traffic shaping from our NetEm configuration on the first NIC, thereafter experiencing network delay from our NetEm configuration on the second NIC.

\Cref{lst:bridge-utils,,lst:bridge-utils-setup,,lst:network-bridge-configuration} demonstrates the installation and configuration neccesary in order to configure the networking bridge. \newline


\begin{lstlisting}[frame=bt,numbers=none,caption={Installing bridge-utils},label={lst:bridge-utils},captionpos=b]
$ apt-get install bridge-utils
\end{lstlisting}

\begin{lstlisting}[frame=bt,numbers=none,caption={Create bridge and assign interfaces},label={lst:bridge-utils-setup},captionpos=b]
$ brctl addbr br0
$ brctl addif br0 eth0 eth1
\end{lstlisting}

Configuring bridging in /etc/network/interfaces

\begin{lstlisting}[caption={Configuration of network interfaces},label={lst:network-bridge-configuration},captionpos=b]
auto lo
iface lo inet loopback

iface eth0 inet dhcp

iface eth1 inet dhcp

iface br0 inet dhcp
  bridge_ports eth0 eth1
\end{lstlisting}


\subsection{Enabling ECN support}
\label{subsubsection:enable-ecn-support}

% quote from https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
On all of our machines used for testing, we have made sure that the ECN setting has been set to 1, which Control use of Explicit Congestion Notification (ECN) by TCP. ECN is used only when both ends of the TCP connection indicate support for it. This feature is useful in avoiding losses due to congestion by allowing supporting routers to signal congestion before having to drop packets. Sysctl \cite{linux-sysctl}.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ sysctl net.ipv4.tcp_ecn
$ sysctl -w net.ipv4.tcp_ecn=1
\end{lstlisting}

\begin{table}[h!]
  \label{tab:tcp-ecn-config}
  \caption{Overview possible values for \textbf{tcp\_ecn}}
  \centering
  \begin{tabularx}{\textwidth}{|Sl|X|} \hline
    \textbf{Value} & \textbf{Description} \\ \hline
    \textbf{0} & {Disable ECN. Neither initiate nor accept ECN.} \\ \hline
    \textbf{1} & {Enable ECN when requested by incoming connections and also request ECN on outgoing connection attempts.} \\ \hline
    \textbf{2} & {Enable ECN when requested by incoming connections but do not request ECN on outgoing connections.} \\ \hline
  \end{tabularx}
\end{table}


\subsection{Disabling TSO and GSO}
\label{subsubsection:disabling-tso-and-gso}

When running our tests, we disabled TSO and GSO with ethtool \cite{linux-ethtool}.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ ethtool -K eth0 tso off
$ ethtool -K eth0 gso off
\end{lstlisting}


\subsection{Tools}
\label{subsection:tools}

The tools depicted in \cref{table:tools} is used to perform tests on the testbed.

\begin{table}[H]
  \centering
  \begin{tabularx}{\textwidth}{|Sl|X|} \hline
    \textbf{Name} & \textbf{Description} \\ \hline
    netem (tc) \cite{linux-netem} & \footnotesize{Network testing tool, traffic shaper} \\ \hline
    D-ITG \cite{linux-d-itg} & \footnotesize{Generate network traffic} \\ \hline
    tcpdump \cite{linux-tcpdump} & \footnotesize{Record TCP statistics} \\ \hline
    Custom script & \footnotesize{User-space application for sending data with DA-LBE enabled} \\ \hline
  \end{tabularx}
  \caption{Tools overview}
  \label{table:tools}
\end{table}


\section{Network emulation}
\label{subsection:network-emulation}

The traffic-shaping computer has an additional Ethernet port (USB adapter). This is used to force the traffic through this computer, which will reduce the throughput and add network delay in order to test the DA-LBE framework.


\subsection{Rate control}
\label{subsubsection:rate-control}

In order to add network delay, and in order to decrease the throughput on the network interface, we use the netem (tc) networking tool. Rate control is achieved with the following command, reducing the throughput through the Ethernet bridge to the destination.

Using \url{https://github.com/magnific0/wondershaper}, commit \textbf{f4405e8}, the traffic shaping computer have been configured with a bandwidth limitation (download) of 20 Mbit/s. This rate limit affects all traffic moving along the path from source to destination because all traffic is passed through the traffic shaping machine.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ wondershaper -a eth1 -d 20480
\end{lstlisting}


\subsection{Network delay}
\label{subsubsection:network-delay-setup}

Network delay is added by issuing the following command with the netem-command.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ tc qdisc add dev eth0 root netem delay 15ms 1ms distribution normal
\end{lstlisting}


\subsection{Explicit Congestion Notification (ECN)}
\label{subsubsection:ecn-configuration}

ECN signals can be added on the rate limiting computer by using the netem- command to generate ECN signals.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ tc qdisc add dev eth0 root netem loss 3% ecn
\end{lstlisting}


\subsection{Configuring Congestion Control (CC) algorithm}
\label{subsubsection:cc-configuration}

The TCP Congestion Control (CC) algorithm can be changed with the following command (TCP Vegas used as example).

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ cat /proc/sys/net/ipv4/tcp_congestion_control
$ echo "vegas" > /proc/sys/net/ipv4/tcp_congestion_control
\end{lstlisting}

A list of available CC modules can be found by issuing the following command.

\begin{lstlisting}[language=bash,frame=bt,numbers=none]
$ ls /lib/modules/`uname -r`/kernel/net/ipv4/
\end{lstlisting}
