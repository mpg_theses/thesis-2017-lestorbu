% Introduction chapter
\chapter{Introduction}
\label{chapter:introduction}

The Internet is much like a road-system which spans across the entire world. The road-system has highways which can carry a lot of traffic as well as smaller roads which can carry less traffic. Roads have interconnections, traffic queues, and border patrol, and similarly the Internet has routers, network queues, and firewalls. Some roads have a high-speed limit, some have a lower speed limit, while the Internet has high bandwidth data links which can carry a lot of data as well as lower bandwidth data links which can carry less data.

While driving, most people are eager to get to their destination as fast as possible, driving as quickly as possible within the speed limit. Other people have a plethora of time, just appreciating the journey, not thinking too much about when they reach their destination - provided they do so within a reasonable amount of time. While comparing the Internet with a road-system, some traffic is less important than others (i.e., freight transport has a lower priority than human transport). Obviously, similarities can be drawn between network traffic and road traffic.

In keeping with the road analogy, this thesis will look into more detail on how traffic of differing urgency can be able to coexist while applying some frame for the yielding traffic to complete within a given soft deadline.

The foundation for this thesis builds on the framework proposal composed by \textbf{\citeauthor{da-lbe-framework}}. The framework proposal includes a solid foundation for the implemented framework. The effectiveness of the approach in the framework proposal is validated by numerical and emulation experiments. The purpose of this thesis is to analyze the framework proposal and assess mechanisms which can provide Less than Best Effort (LBE) behaviour on an arbitrary Congestion Control (CC) in Linux.

% TODO: consider adding (some of) this
% The Transmission Control Protocol (TCP) provides a congestion control mechanism. This mechanism crucially enables TCP to dynamically adjust its sending rate to actual network conditions, the aim being to send as fast as possible without causing disruptions (i.e. congestion).


\section{Problem statement}
\label{section:problem-statement}

Networks use congestion avoidance and congestion control as techniques to limit network congestion and avoid collapse. CCs is constantly probing for bandwidth in order to know when to increase or decrease transmission rate. The individual CCs define how it should react to network congestion, and what network congestion indications it should react to. An LBE service reacts to network congestion earlier than standard TCP would, which results in a lower impact on non-LBE network traffic when bandwidth is shared by multiple competing entities.

Commonly, network traffic is carried out using a Best Effort (BE) service in order to  maximize their transmission rate. Network traffic carried out by an LBE service is yielding towards network traffic using a BE service. LBE behaviour is commonly called a scavenger service, based on the fact that it is utilizing unused network bandwidth. Providing LBE service entails only using bandwidth which is unused by other consumers on the network, particularly non-LBE traffic. In order for LBE to use the unused bandwidth, it should yield to non-LBE traffic, adapting to network changes and traffic load on the network. When an LBE service observes a decrease in network congestion it will increase its transmission rate. Similarly, an LBE service will decrease its transmission rate when it observes an increase in congestion indications, in order to allow for traffic of differing urgency to maximize their transmission rate, resulting in a more efficient utilization of network bandwidth.

% LBE CC algorithms should react to congestion by quickly reducing the sender's data rate, and should try to keep network buffer usage low so as to keep queuing delays to a minimum.

LBE does not consider any notion of timeliness requirements when yielding for traffic of differing urgency. LBE services could be starved by BE flows, causing the LBE services to yield indefinitely. This can be remedied by adding the notion of time to LBE, coined Deadline Aware Less than Best Effort Services (DA-LBE). Adding notion of time to an LBE service will allow it to adjust how early it reacts to network congestion. A DA-LBE service will be able to conform to standard TCP BE services if it needs to in order to complete within the soft deadline.

The purpose of this thesis is to implement Deadline Aware Less than Best Effort support in the Linux Kernel. This thesis will reasearch how DA-LBE behaviour can be imposed on an arbitrary end-to-end CC. End-to-end CC has algorithms running on the end-hosts, as opposed to the alternative of having some sort of priority scheduling in routers. This thesis will research the possibility to adjust the behaviour of a CC by adjusting the transmission rate without having to modify the source code of CC or dropping packets to trigger congestion avoidance. The DA-LBE behaviour should provide timeliness constraints on LBE mechanisms in order to complete within a soft deadline (i.e., I want it to be finished by tomorrow at 3PM). Ideally, the LBE mechanisms should be able to be controlled on a per-socket level, as opposed to a system-wide configuration.

The Deadline Aware, Less than Best Effort (DA-LBE) framework, implemented in Linux should provide the following functionality (excerpt from \cite{da-lbe-framework}):

\begin{itemize}
  \item Keep disruption of concurrent BE interactive services to a minimum.
  \item Add a timeliness constraint to the transport, i.e., the transfer should be finished by a soft deadline to fit in with other network activities and to ensure the timely correctness of replicated data.
\end{itemize}

To achieve the functionality described above, it should be able to dynamically adjust its aggressiveness in competing with BE traffic from that of a scavenger-type service up to that of a BE-type service. The DA-LBE framework would be required to provide information which shows the current state of the network in regards to network congestion and bandwidth utilization. In order to support modification of the transmission rate of an arbitrary CC, mechanisms which will change the perception of network congestion indications is required. The DA-LBE framework must provide mechanism which are applicable to commonly used network congestion indicators such as loss, queueing delay and explicit congestion notifications.

The DA-LBE framework will require Linux Kernel support in order for the existing CCs to be adapted to this use. This thesis will investigate how the problem statement described above can be implemented and organized in Linux.

% it should be able to dynamically adjust its aggressiveness in competing with BE traffic from that of a scavenger-type service up to that of a BE-type service.

% The implementation of DA-LBE is presented in this thesis, and it shows how the framework introduces control structures, which lets an arbitrary CC support Less than Best Effort with Deadlines.

% The goal is being able to enable yielding on an arbitrary CC by reducing the transmission rate. A key point is being able to do so without dropping packets in order to reduce the transmission rate.


\section{Motivation}
\label{section:motivation}

The DA-LBE framework is is a contribution to a New Evolutive API and Transport-Layer (NEAT) Architecture for the Internet. NEAT is funded by the European Union's Horizon 2020 research and innovation programme. Applications which do not carry strong capacity or latency constraints, such as backups, could use a Less than Best Effort (LBE) transport protocol as an alternative to best-effort TCP. This could minimize their potential to disrupt network flows with a higher significance. Existing LBE CCs, such as LEDBAT, enforce LBE behaviour without any timeliness requirements regarding completion time.

The key motivational points of this thesis is to:

\begin{itemize}
  \item Impose LBE behaviour with a notion of time in order to support soft deadlines by dynamically adjusting the aggressiveness when competing with BE network traffic. Soft deadlines can prevent network starvation \cite{5735831} and latecomer unfairness \cite{6226797} as is observed in an LBE implementation such as LEDABT.
  \item Support a wide range of CCs as opposed to attempt to develop a one-size-fits-all CC. This will allow application developers to enable deadline aware LBE behaviour on new or existing applications with minimal configuration requirements.
\end{itemize}

The motivation for implementing a Deadline Aware, Less than Best Effort (DA-LBE) framework is to be able to LBE haviour on an arbitrary CC with the notion of time. DA-LBE could be used to balance non-intrusiveness and respect of timeliness constraints. A key use for an LBE transport service is data backup applications, by limiting the impact of a backup service towards network flows of higher significance (e.g. streaming services). Backup applications does generally have lower throughput and delay requirements compared to traffic such as video conferencing, streaming services, etc.

Applications which do not have any specific throughput or delay requirements will have the ability to take advantage of periods when the network has low bandwidth utilization. The DA-LBE framework could be used by such an application in order to incrase network utilization. No deadline-aware LBE CC methods have ever been proposed in literature \cite{NEAT}. An LBE which includes a notion of time will provide the ability to request an arbitrary network transmission to perform Lower than Best Effort (LBE) (see \cref{section:lbe}). DA-LBE solves the LBE starvation found in LEDBAT \cite{5735831} by implementing deadline awareness.


\section{Research questions}
\label{section:research-questions}

This thesis will discuss the following research questions.

\begin{itemize}
  \item Is it possible to apply mechanisms in the Linux Kernel which can reduce the transmission rate on an arbitrary CC without causing loss?
  \item Can traffic of differing urgency coexist while applying some frame for the yielding traffic to complete within a given soft deadline?
  \item Can the former items be achieve on a per-socket basis in Linux?
\end{itemize}


\section{Organization}
\label{section:organization}

This thesis is organised by first introducing the \textbf{\nameref{chapter:background}} chapter providing information useful in order to understand the DA-LBE framework and how it is implemented in the Linux Kernel. Next, the \textbf{\nameref{chapter:implementation}} chapter presents the implementation of the DA-LBE kernel framework in Linux and explores how it has been structured and organized. Followed by the \textbf{\nameref{chapter:experimental-setup}} chapter, which explores the technical configuration for the experimental environment, where experiements and measurements have been performed and the purpose of the different experiments that have been carried out in this thesis. The experimental results is presented in the \textbf{\nameref{chapter:experimental-evaluation}} chapter, which evaluates the outcome of the experiments that was carried out. The thesis is concluded in the \textbf{\nameref{chapter:conclusion}} chapter, where the effectiveness of the DA-LBE framework implementation and future work is discussed.
