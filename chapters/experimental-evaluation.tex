% Results chaptera
\chapter{Experimental evaluation}
\label{chapter:experimental-evaluation}

This chapter presents the evaluation of the experimental results of the experiments that have been carried out in order to evaluate the DA-LBE implementation in the Linux Kernel. The environmental setup is described in detail in \cref{chapter:experimental-setup}.


\section{Experimental objective}

The experimental objective of the experiments is to assess the effectiveness of the DA-LBE framework implementation in the Linux Kernel. The mechanisms of the DA-LBE framework will be evaluated alongside comparable mechanisms in existing network utilities. The objective is to provide experimental results which confirm the DA-LBE framework ability to adjust the transmission rate on an arbitarry CC. The CC algorithms which the DA-LBE framework has been tested on is the loss-based CC TCP CUBIC and the delay-based CC TCP Vegas.


\section{Experimental configuration}

A number of experiments have been configured in order to evaluate the effectiveness of the DA-LBE mechanisms implemented in the Linux Kernel. Some experiments delineate an ordinary network transmission, where no DA-LBE mechanism influence the transmission. These ordinary network transmissions acts as a comparison sample for experiments where DA-LBE mechanisms or comparable mechanisms influence the transmission.

\Cref{table:shared-config} shows the configuration which is shared across all experiments.

\begin{table}[H]
  \centering
  \begin{tabularx}{\textwidth}{ |X|X|X| } \hline
    \textbf{Bandwidth} & \textbf{Queueing delay} & \textbf{Transfer size}  \\ \hline
    20 Mbit/s & 30 ms $\pm$ 1 ms & 100MB \\ \hline
  \end{tabularx}
  \caption{Shared configuration across all experiments}
  \label{table:shared-config}
\end{table}

The experimental results presented in \cref{experiment:netem-ecn,,experiment:phantom-ecn,,experiment:ignoring-ecn} is configured as a network transfer which enables the respective mechanism halfway through the transmission. The objective of configuring the test in this manner, is to make it easier to recognize how the enabled mechanism affect throughput, congestion window and round-trip time. See \cref{figure:experiment-configuration} in order to recognize how the experiments mentioned above have been carried out.

\begin{figure}[H]
  \includegraphics[keepaspectratio]{graphics/misc/experiment-configuration}
  \caption[Experiment configuration - Transfer]{The experiment configuration enables the respective mechanism halfway through the transmission. This will make it easier to recognize how the mechanism affect throughput, congestion window and round-trip time.}
  \label{figure:experiment-configuration}
\end{figure}


\section{Experimental results}

The experimental results of each experiment are presented below. Different mechanisms are separated into their own section. The key point of the experimental results is to provide information about how the different mechanisms affect the network transmission. Throughput, Congestion Window and Round-trip Time (RTT) of the experimental results is vizualized over time. The collection rate interval (sample length) of the plotted graphs is at 1 second.


\subsection{Comparison sample}
\label{experiment:comparison-sample}

The comparison sample using TCP CUBIC is shown in \cref{experiment:comparison-sample-cubic}, and the comparison sample using TCP Vegas is shown in \cref{experiment:comparison-sample-vegas}.

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/reference/cubic/cubic-ref-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/reference/cubic/cubic-ref-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/reference/cubic/cubic-ref-rtt}
  \caption[Comparison sample - TCP CUBIC]{Comparison sample - TCP CUBIC (\cref{experiment:appendix-cubic-reference-throughput,,experiment:appendix-cubic-reference-cwnd,,experiment:appendix-cubic-reference-rtt})}
  \label{experiment:comparison-sample-cubic}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/reference/vegas/vegas-ref-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/reference/vegas/vegas-ref-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/reference/vegas/vegas-ref-rtt}
  \caption[Comparison sample - TCP Vegas]{Comparison sample - TCP Vegas (\cref{experiment:appendix-vegas-reference-throughput,,experiment:appendix-vegas-reference-cwnd,,experiment:appendix-vegas-reference-rtt})}
  \label{experiment:comparison-sample-vegas}
\end{figure}

The comparison sample of TCP CUBIC and TCP Vegas are very similar, which was expected. It can be seen from the throughput graphs that TCP CUBIC achieves a smoother throughput curve than that of TCP Vegas because of the implementation characteristics of TCP CUBIC. TCP Vegas is able to more quickly respond to network changes while probing for bandwidth, giving a throughput pattern with more spikes than that of TCP CUBIC.


\subsection{NetEm ECN}
\label{experiment:netem-ecn}

The experiment configured with NetEm with 2.5\% ECN probability is presented in this section. The TCP CUBIC results are shown in \cref{experiment:netem-ecn-cubic}, and the TCP Vegas results are shown in \cref{experiment:netem-ecn-vegas}.

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/cubic/cubic-netem-ecn-2_5-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/cubic/cubic-netem-ecn-2_5-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/cubic/cubic-netem-ecn-2_5-rtt}
  \caption[NetEm ECN 2.5\% - TCP CUBIC]{NetEm ECN - TCP Vegas - 2.5\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-cubic-netem-ecn-2-5-throughput,,experiment:appendix-cubic-netem-ecn-2-5-cwnd,,experiment:appendix-cubic-netem-ecn-2-5-rtt})}
  \label{experiment:netem-ecn-cubic}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/vegas/vegas-netem-ecn-2_5-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/vegas/vegas-netem-ecn-2_5-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/netem-ecn/vegas/vegas-netem-ecn-2_5-rtt}
  \caption[NetEm ECN 2.5\% - TCP Vegas]{NetEm ECN - TCP Vegas - 2.5\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-vegas-netem-ecn-2-5-throughput,,experiment:appendix-vegas-netem-ecn-2-5-cwnd,,experiment:appendix-vegas-netem-ecn-2-5-rtt})}
  \label{experiment:netem-ecn-vegas}
\end{figure}

NetEm is able to efficiently lower the throughput on both TCP CUBIC and TCP Vegas. The average throughput with the NetEm mechanism enabled is well below 500 KB/s with a 2.5\% ECN probability.


\subsection{Phantom ECN}
\label{experiment:phantom-ecn}

The experiment configured with DA-LBE with 2.5\% Phantom ECN probability is presented in this section. The TCP CUBIC results are shown in \cref{experiment:phantom-ecn-cubic-2-5}, and the TCP Vegas results are shown in \cref{experiment:phantom-ecn-vegas-2-5}.

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/2_5/cubic-ph-ecn-2_5-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/2_5/cubic-ph-ecn-2_5-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/2_5/cubic-ph-ecn-2_5-rtt}
  \caption[Phantom ECN 2.5\% - TCP CUBIC]{Phantom ECN - TCP CUBIC - 2.5\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-cubic-phantom-ecn-2-5-throughput,,experiment:appendix-cubic-phantom-ecn-2-5-cwnd,,experiment:appendix-cubic-phantom-ecn-2-5-rtt})}
  \label{experiment:phantom-ecn-cubic-2-5}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/vegas/vegas-ph-ecn-2_5-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/vegas/vegas-ph-ecn-2_5-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/vegas/vegas-ph-ecn-2_5-rtt}
  \caption[Phantom ECN 2.5\% - TCP Vegas]{Phantom ECN - TCP Vegas - 2.5\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-vegas-phantom-ecn-2-5-throughput,,experiment:appendix-vegas-phantom-ecn-2-5-cwnd,,experiment:appendix-vegas-phantom-ecn-2-5-rtt})}
  \label{experiment:phantom-ecn-vegas-2-5}
\end{figure}

DA-LBE is able to lower the throughput on both TCP CUBIC and TCP Vegas using Phantom ECN. When comparing the outcome of the DA-LBE Phantom ECN with the NetEm ECN, we observe that with the same probability value, the transmission rate is lower in the NetEm experimental results. The average throughput with the DA-LBE Phantom ECN mechanism enabled is well above 1000 KB/s with a 2.5\% ECN probability using TCP CUBIC. The average throughput with the DA-LBE Phantom ECN mechanism enabled is just below 1000 KB/s with a 2.5\% ECN probability using TCP Vegas. TCP Vegas seem to be reacting slightly better to the generation of the DA-LBE Phantom ECN signals.


\subsubsection{Increasing the probability}

In order to show how DA-LBE Phantom ECN operate on different values of probability, we have executed the Phantom ECN experiments using TCP CUBIC with 5\% Phantom ECN probability, shown in \cref{experiment:phantom-ecn-cubic-5}, and with 10\% Phantom ECN probability, shown in \cref{experiment:phantom-ecn-cubic-10}.

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/5/cubic-ph-ecn-5-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/5/cubic-ph-ecn-5-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/5/cubic-ph-ecn-5-rtt}
  \caption[Phantom ECN 5\% - TCP CUBIC]{Phantom ECN - TCP CUBIC - 5\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-cubic-phantom-ecn-5-throughput,,experiment:appendix-cubic-phantom-ecn-5-cwnd,,experiment:appendix-cubic-phantom-ecn-5-rtt})}
  \label{experiment:phantom-ecn-cubic-5}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=0.95\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/10/cubic-ph-ecn-10-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/10/cubic-ph-ecn-10-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/phantom-ecn/cubic/10/cubic-ph-ecn-10-rtt}
  \caption[Phantom ECN 10\% - TCP CUBIC]{Phantom ECN - TCP CUBIC - 10\% ECN - Throughput, CWND and RTT (\cref{experiment:appendix-cubic-phantom-ecn-10-throughput,,experiment:appendix-cubic-phantom-ecn-10-cwnd,,experiment:appendix-cubic-phantom-ecn-10-rtt})}
  \label{experiment:phantom-ecn-cubic-10}
\end{figure}

The increased probability of DA-LBE Phantom ECN generation correlates nicely to the reducal in throughput shown in the graphs.


\subsection{Ignoring ECN}
\label{experiment:ignoring-ecn}

After having tested the NetEm ECN with a percentage of 2.5\% ECN probability on TCP CUBIC, we want to make sure that the DA-LBE mechanism ignoring ECN signal is able to working properly. The objective is to compare the experimental result of \cref{experiment:ignoring-ecn-cubic} to show a higher transmission rate than \cref{experiment:netem-ecn-cubic}.

\begin{table}[H]
  \centering
  \begin{tabularx}{\textwidth}{ |X|X|X| } \hline
    \textbf{NetEm ECN} & \textbf{Phantom ECN} & \textbf{Ignore ECN}  \\ \hline
    2.5\% & 0\% & 10\% \\ \hline
  \end{tabularx}
  \caption{Ignoring ECN configuration}
  \label{table:ignore-ecn-config}
\end{table}

\begin{figure}[H]
  \includegraphics[width=\textwidth,keepaspectratio]{graphics/experiments/ignore-ecn/cubic/cubic-ignore-ecn-throughput}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/ignore-ecn/cubic/cubic-ignore-ecn-cwnd}
  \includegraphics[width=0.5\textwidth,keepaspectratio]{graphics/experiments/ignore-ecn/cubic/cubic-ignore-ecn-rtt}
  \caption[NetEm ECN - TCP CUBIC]{Ignoring ECN - TCP CUBIC - 2.5\% NetEm ECN and 10\% DA-LBE ignore ECN - Throughput, CWND and RTT (\cref{experiment:appendix-cubic-ignore-ecn-throughput,,experiment:appendix-cubic-ignore-ecn-cwnd,,experiment:appendix-cubic-ignore-ecn-rtt})}
  \label{experiment:ignoring-ecn-cubic}
\end{figure}

When the experimental result of \cref{experiment:ignoring-ecn-cubic} is compared with the experimental result of \cref{experiment:netem-ecn-cubic}, a notable difference in throughput is observed. The NetEm 2.5\% ECN configuration has an average transmission rate well below 500 KB/s after the mechanism was enabled. The NetEm 2.5\% ECN and DA-LBE Ignore 10\% ECN has an average transmission rate above 1000 KB/s after the mechanisms was enabled. This show that the DA-LBE mechanism of ignoring ECN signals is working.


% \subsection{Ignoring loss}
% \label{experiment:ignoring-loss}

% \subsection{Phantom delay}
% \label{experiment:ignoring-loss}

\section{Conclusion}

The DA-LBE framework is able to reduce the transmission rate effectively on both TCP CUBIC and TCP Vegas. The DA-LBE implementation of Phantom ECN is less efficient in reducing the transmission rate compared to NetEm ECN given the same probailistic rate of ECN generation. Phantom ECN needs a higher probability in order to produce the same reducal in transmission rate as NetEm ECN given a probabilistic value. The DA-LBE Phantom ECN experimental results show that increasing the probability correlates nicely to the reducal of the transmission rate.

The DA-LBE Phantom ECN experimental results show that TCP Vegas reacts more aggressively to the Phantom ECN generation, producing a lower transmission rate than TCP CUBIC with the same probability of Phantom ECN generation.

The experimental results of the DA-LBE mechanism of ignoring ECN show that it is able to probabilistically not reduce the transmission rate when experiencing NetEm ECN.
