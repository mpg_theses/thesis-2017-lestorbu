\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{lestorbu}[2018/02/01 Class for master thesis]

%
% Set default values for the variables used by our implementation of \maketitle
%
\newcommand{\SubTitle}{}
\newcommand{\DegreeType}{Programming and Networks}
\newcommand{\Faculty}{ }
\newcommand{\Department}{}
\newcommand{\CopyrightNotice}{}
\newcommand{\ISBNPrinted}{N/A}
\newcommand{\ISBNElectronic}{N/A}
\newcommand{\SerialNumber}{\number\year:N/A}
\newcommand{\Month}{\ifcase\month\or
	January\or February\or March\or April\or May\or June\or
	July\or August\or September\or October\or November\or December\fi}
\newcommand{\Year}{\number\year}
\newcommand{\Day}{15th}
\renewcommand{\@author}{Lars Erik Storbukås}
\renewcommand{\@title}{Title}

%
% Set the default values for margin settings
%
\newcommand{\GeometryOptions}{margin=20mm,top=15mm,bindingoffset=10mm}
\newcommand{\CropOptions}{}
\newcommand{\CropLandscapeFlag}{}

%
% Commodity command to give two different versions of code, the first for printout mode
% and the second for screen mode. Useful for large pictures and layouts in absolute units.
%
\newcommand{\printandscreen}[2]{\ifthenelse{\isundefined{\ScreenMode}}{#1}{#2}}

%
% Process options passed to the class
%
\DeclareOption{a4crop}{\renewcommand{\CropOptions}{cam,a4,center}}
\DeclareOption{draft}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{final}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{screen}{
	\renewcommand{\CropLandscapeFlag}{landscape}
	\renewcommand{\GeometryOptions}{margin=10mm,screen}
	\newcommand{\ScreenMode}{true}
}
\DeclareOption*{\PackageWarning{lestorbu}{Unknown option `\CurrentOptionÂ´.}}
\ProcessOptions\relax

\LoadClass[b5paper,11pt,twoside,openright,onecolumn,titlepage]{book}

% Set appropriate margins
\RequirePackage[\GeometryOptions,includeheadfoot,headheight=14.5pt]{geometry}

% Set cropping marks, if a4crop has been specified.
\RequirePackage[\CropOptions,\CropLandscapeFlag]{crop}

% Necessary to set the headers; notice that right and left headers are swapped.
\RequirePackage{fancyhdr}

% Used in some conditional statements
\RequirePackage{ifthen}

% Necessary to include the logo
\RequirePackage{graphicx}

%
% Define the commands to set the variables used by \maketitle
%
\newcommand{\subtitle}[1]{\renewcommand{\SubTitle}{#1}}
\newcommand{\degreetype}[1]{\renewcommand{\DegreeType}{#1}}
\newcommand{\faculty}[1]{\renewcommand{\Faculty}{#1}}
\newcommand{\department}[1]{\renewcommand{\Department}{#1}}
\newcommand{\copyrightnotice}[1]{\renewcommand{\CopyrightNotice}{#1}}
\newcommand{\isbnprinted}[1]{\renewcommand{\ISBNPrinted}{#1}}
\newcommand{\isbnelectronic}[1]{\renewcommand{\ISBNElectronic}{#1}}
\newcommand{\serialnumber}[1]{\renewcommand{\SerialNumber}{#1}}
\newcommand{\setyear}[1]{\renewcommand{\Year}{#1}}
\newcommand{\setmonth}[1]{\renewcommand{\Month}{#1}}

%
% Override the \maketitle command
%
\renewcommand{\maketitle}{
	\begin{titlepage}
	\parindent=0cm
	\addtolength{\parskip}{\baselineskip}
	{\Huge \@author}
	\printandscreen{\vspace{2cm}}{\vspace{0.5cm}}

	{\Huge \textbf{\@title}}

  {\LARGE \SubTitle}

	\vfill

	{\large Thesis submitted for the degree of \\ Master in \DegreeType

	\Month\ \Day, \Year

	University of Oslo\\
	\Department\ \\
	\Faculty}
	\printandscreen{\vspace{2cm}}{\vspace{0.75cm}}

	\includegraphics[width=0.6\textwidth]{graphics/icons/simula_logo}

	\includegraphics[width=0.6\textwidth]{graphics/icons/uio_logo}

	\end{titlepage}
}

\AtBeginDocument{
	\pagestyle{fancy}
	\renewcommand{\headrulewidth}{0.4pt}
	\renewcommand{\sectionmark}[1]{\markright{\textbf{\thesection.\ #1}}}
	\renewcommand{\chaptermark}[1]{\markboth{\textbf{#1}}{}}
	\rhead{\nouppercase{\leftmark}}
	\lhead{\nouppercase{\rightmark}}
	\printandscreen{
		\fancyhead[LE,RO]{\textbf{\thepage}}
	}{
		\fancyhead[R]{\textbf{\thepage}}
	}
	\fancyfoot[C]{}
}
